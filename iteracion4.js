// Iteracion #4
const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];
console.log(avengers[0]);

avengers[0]='IRONMAN';

console.log(avengers.length+1);

const rickAndMortyCharacters = ["Rick", "Beth", "Jerry"];
rickAndMortyCharacters.push("Morty","Summer")
console.log(rickAndMortyCharacters[4]);

rickAndMortyCharacters.push("Lapiz Lopez");
rickAndMortyCharacters.pop();
console.log(rickAndMortyCharacters[0] + ' '+rickAndMortyCharacters[4]);

rickAndMortyCharacters.push("Lapiz Lopez");
rickAndMortyCharacters.splice(1,1);
console.log(rickAndMortyCharacters);